#include <devicematrix.h>

namespace krn {
void run_kernel(DeviceMatrix temperature, DeviceMatrix temperature_1,
                int sourcex, int sourcey);
void reset_kernel(DeviceMatrix temperature);
void visualize_kernel(uchar4 *framebufferOUT, DeviceMatrix temperature);
} // namespace krn
