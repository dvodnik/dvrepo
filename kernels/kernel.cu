#include <kernel.h>

__device__ float get(DeviceMatrix &data, int x, int y) {
  if (x < 0)
    x = 0;
  if (y < 0)
    y = 0;
  if (x >= data.width)
    x = data.width - 1;
  if (y >= data.height)
    y = data.height - 1;
  // if (x < 0) return 0.0f;
  // if (y < 0) return 0.0f;
  // if (x >= data.width) return 0.0f;
  // if (y >= data.height) return 0.0f;
  return data.data[x + y * data.width];
}

__device__ float &set(DeviceMatrix &data, int x, int y) {
  return data.data[x + y * data.width];
}

__global__ void kernel(DeviceMatrix temperature, DeviceMatrix temperature_1,
                       int sourcex, int sourcey) {
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;

  if (x < temperature.width && y < temperature.height) {
    float a = 0.2;

    int w = 5;
    if (x > sourcex - w)
      if (x < sourcex + w)
        if (y > sourcey - w)
          if (y < sourcey + w)
    	    set(temperature, x, y) = 1.0f;

    	set(temperature_1, x, y) = 
    	    (1-4*a) * get(temperature, x, y)
    	    + a * get(temperature, x-1, y)
    	    + a * get(temperature, x+1, y)
    	    + a * get(temperature, x, y-1)
    	    + a * get(temperature, x, y+1);
	}
}

__global__ void reset(DeviceMatrix temperature) {
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;

  if (x < temperature.width && y < temperature.height) {
    set(temperature, x, y) = 0.0f;
  }
}

__global__ void visualize(uchar4 *framebufferOUT, DeviceMatrix temperature) {
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;
  int offset = y * blockDim.x * gridDim.x + x;

  if (x < temperature.width && y < temperature.height) {
    float xxx = 2 * get(temperature, x, y);
    if (xxx > 1.0f)
      framebufferOUT[offset].x = 255;
    else
      framebufferOUT[offset].x = int(255 * xxx / 10) * 10;
    framebufferOUT[offset].y = 255 * get(temperature, x, y);
    framebufferOUT[offset].z = 255 * get(temperature, x, y);
    framebufferOUT[offset].w = 255 * get(temperature, x, y);
  }
}

namespace krn {

void run_kernel(DeviceMatrix in, DeviceMatrix out, int sourcex, int sourcey) {
  dim3 blockSize(32, 16);
  dim3 gridSize((in.width + blockSize.x - 1) / blockSize.x,
                (in.height + blockSize.y - 1) / blockSize.y);
  kernel<<<gridSize, blockSize>>>(in, out, sourcex, sourcey);
}

void reset_kernel(DeviceMatrix in) {
  dim3 blockSize(32, 16);
  dim3 gridSize((in.width + blockSize.x - 1) / blockSize.x,
                (in.height + blockSize.y - 1) / blockSize.y);
  reset<<<gridSize, blockSize>>>(in);
}

void visualize_kernel(uchar4 *framebufferOUT, DeviceMatrix in) {
  dim3 blockSize(32, 16);
  dim3 gridSize((in.width + blockSize.x - 1) / blockSize.x,
                (in.height + blockSize.y - 1) / blockSize.y);
  visualize<<<gridSize, blockSize>>>(framebufferOUT, in);
}

} // namespace krn
