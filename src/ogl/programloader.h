
typedef unsigned int GLuint;

namespace ogl {
class ProgramLoader {
public:
  static GLuint loadShader(const char *source, GLuint type);
  static GLuint loadProgram(const char *vsSource, const char *fsSource);
  };
}
