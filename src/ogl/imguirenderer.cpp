#include <GL/glew.h>
#include <imguirenderer.h>
#include <programloader.h>

namespace ogl {
const char *vsSource = R"glsl(
#version 330 core
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 uv;
layout (location = 2) in vec4 color;

uniform mat4 Ortho;

out vec4 cl;
out vec2 texuv;

void main() {
	cl = color;
	texuv = uv;
    gl_Position = Ortho * vec4(position, 0.0f, 1.0f);
}
)glsl";

const char *fsSource = R"glsl(
#version 330 core
uniform sampler2D Texture;

in vec4 cl;
in vec2 texuv;
out vec4 outColor;

void main() {
    outColor = cl * texture( Texture, texuv.st);
}
)glsl";

GLuint ImGuiRenderer::init(unsigned char *pixels, int w, int h) {
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
  glGenTextures(1, &fontTextureID);
  glBindTexture(GL_TEXTURE_2D, fontTextureID);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE,
               pixels);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

  program = ProgramLoader::loadProgram(vsSource, fsSource);
  matrix = glGetUniformLocation(program, "Ortho");
  textureLoc = glGetUniformLocation(program, "Texture");
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &elements);

  return fontTextureID;
}

void ImGuiRenderer::draw(ImDrawData *draw_data, int width, int height) {
  const float ortho_projection[4][4] = {
      {2.0f / width, 0.0f, 0.0f, 0.0f},
      {0.0f, 2.0f / -height, 0.0f, 0.0f},
      {0.0f, 0.0f, -1.0f, 0.0f},
      {-1.0f, 1.0f, 0.0f, 1.0f},
  };

  glEnable(GL_BLEND);
  glBlendEquation(GL_FUNC_ADD);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_SCISSOR_TEST);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  // glBindSampler(0, 0); // Rely on combined texture/sampler state.

  glUseProgram(program);
  // glUniform1i(textureLoc, 0);
  glUniformMatrix4fv(matrix, 1, GL_FALSE, &ortho_projection[0][0]);
  glBindVertexArray(VAO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert),
                        (GLvoid *)IM_OFFSETOF(ImDrawVert, pos));
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert),
                        (GLvoid *)IM_OFFSETOF(ImDrawVert, uv));
  glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert),
                        (GLvoid *)IM_OFFSETOF(ImDrawVert, col));
  for (int n = 0; n < draw_data->CmdListsCount; n++) {
    const ImDrawList *cmd_list = draw_data->CmdLists[n];
    const ImDrawIdx *idx_buffer_offset = nullptr;

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER,
                 (GLsizeiptr)cmd_list->VtxBuffer.Size * sizeof(ImDrawVert),
                 (const GLvoid *)cmd_list->VtxBuffer.Data, GL_STREAM_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 (GLsizeiptr)cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx),
                 (const GLvoid *)cmd_list->IdxBuffer.Data, GL_STREAM_DRAW);

    for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++) {
      const ImDrawCmd *pcmd = &cmd_list->CmdBuffer[cmd_i];
      if (pcmd->UserCallback) {
        pcmd->UserCallback(cmd_list, pcmd);
      } else {
        glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
        glScissor((int)pcmd->ClipRect.x, (int)(height - pcmd->ClipRect.w),
                  (int)(pcmd->ClipRect.z - pcmd->ClipRect.x),
                  (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
        glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount,
                       sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT
                                              : GL_UNSIGNED_INT,
                       idx_buffer_offset);
      }
      idx_buffer_offset += pcmd->ElemCount;
    }
  }
}
} // namespace ogl
