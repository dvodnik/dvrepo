#include <imgui.h>

namespace ogl {
typedef unsigned int GLuint;
class ImGuiRenderer {
  GLuint program, matrix, textureLoc;
  GLuint VAO, VBO, elements;
  GLuint fontTextureID;

public:
  GLuint init(unsigned char *pixels, int w, int h);
  void draw(ImDrawData *draw_data, int width, int height);
};
} // namespace ogl
