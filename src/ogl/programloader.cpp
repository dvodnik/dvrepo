#include <GL/glew.h>
#include <programloader.h>
#include <cstdio>
#include <cstdlib>

namespace ogl {
GLuint ProgramLoader::loadShader(const char *source, GLuint type) {
  GLuint shader = glCreateShader(type);
  glShaderSource(shader, 1, &source, nullptr);
  glCompileShader(shader);

  GLint success = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
  if (!success) {
    GLchar *errorLog;
    printf("Shader compilation error:\n");

    GLint logSize = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
    errorLog = (GLchar *)malloc(logSize * sizeof(GLchar));
    glGetShaderInfoLog(shader, logSize, &logSize, errorLog);

    printf("%s\n", errorLog);

    exit(1);
  }

  return shader;
}

GLuint ProgramLoader::loadProgram(const char *vsSource, const char *fsSource) {
  GLuint vShader = loadShader(vsSource, GL_VERTEX_SHADER);
  GLuint fShader = loadShader(fsSource, GL_FRAGMENT_SHADER);

  GLuint program = glCreateProgram();
  glAttachShader(program, vShader);
  glAttachShader(program, fShader);

  glLinkProgram(program);
  return program;
}
} // namespace ogl
