#include <heattransfer_gui.h>
#include <imgui.h>
#include <cstdio>
#include <cstdlib>

namespace htr {
void HTGui::init(int w, int h) {
  ImGui::CreateContext();
  ImGuiIO &io = ImGui::GetIO();
  io.DisplaySize.x = w;
  io.DisplaySize.y = h;

  unsigned char *pixels;
  int width, height;
  io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

  auto fontTextureID = imguirenderer.init(pixels, width, height);

  io.Fonts->TexID = (void *)(__intptr_t)fontTextureID;
}

void HTGui::update(ImVec2 mouse_pos, bool mouse_button_0, bool mouse_button_1) {
  ImGuiIO &io = ImGui::GetIO();
  io.DeltaTime = 1.0f / 60.0f;
  io.MousePos = mouse_pos;
  io.MouseDown[0] = mouse_button_0;
  io.MouseDown[1] = mouse_button_1;

  ImGui::NewFrame();
  ImGui::Text(
      "Hello, world!"); // Display some text (you can use a format string too)
}

void HTGui::draw(int width, int height) {
  ImGui::Render();
  ImDrawData *draw_data = ImGui::GetDrawData();

  imguirenderer.draw(draw_data, width, height);
}
} // namespace htr
