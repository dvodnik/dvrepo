#include <imgui.h>
#include <imguirenderer.h>

namespace htr {
class HTGui {
  ogl::ImGuiRenderer imguirenderer;

public:
  void init(int w, int h);
  void update(ImVec2 mouse_pos, bool mouse_button_0, bool mouse_button_1);
  void draw(int width, int height);
};
} // namespace htr
