#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cstring>
#include <cuda_gl_interop.h>
#include <cuda_runtime.h>
#include <iostream>

#include <heattransfer_gui.h>
#include <imgui.h>
#include <heattransfer.h>

const int WIDTH = 1280;
const int HEIGHT = 720;

int main(int /*argc*/, char ** /*argv*/) {
  cudaDeviceProp prop = {0};
  int dev;

  memset(&prop, 0, sizeof(cudaDeviceProp));
  prop.major = 1;
  prop.minor = 0;
  cudaChooseDevice(&dev, &prop);

  cudaGLSetGLDevice(dev);

  if (glfwInit() == 0) {
    printf("GLFW initialization failed.\n");
    exit(1);
  }

  GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "", nullptr, nullptr);
  if (window == nullptr) {
    printf("Window creation failed.\n");
    exit(1);
  }
  glfwMakeContextCurrent(window);

  glewExperimental = 0u;
  if (glewInit() != GLEW_OK) {
    printf("GLEW initialization failed.");
    exit(1);
  }

  htr::HeatTransfer htransfer(WIDTH, HEIGHT);
  htr::HTGui htGui;
  htGui.init(WIDTH, HEIGHT);

  do {
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    htGui.update(
        ImVec2(xpos, ypos),
        glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_TRUE, false);
    htransfer.update(xpos, HEIGHT - ypos);

    htransfer.draw();
    htGui.draw(WIDTH, HEIGHT);

    glfwSwapBuffers(window);
    glfwPollEvents();
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
      htransfer.reset();
    }
  } while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           (glfwWindowShouldClose(window) == 0));
}
