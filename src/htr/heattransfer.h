#include <GL/glew.h>
#include <cuda_gl_interop.h>

#include <devicematrix.h>

namespace htr {
class HeatTransfer {
  int iterationsPerFrame = 128;
  int _width, _height;

  DeviceMatrix temperature;
  DeviceMatrix temperature_1;

  GLuint pixelBuffer;
  cudaGraphicsResource *cudaMappedBuffer;

public:
  HeatTransfer(int width, int height);
  void reset();
  void update(int sourcex, int sourcey);
  void draw();
};
} // namespace htr
