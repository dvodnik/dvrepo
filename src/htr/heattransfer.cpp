#include <GL/glew.h>
#include <cuda_runtime.h>
#include <heattransfer.h>
#include <kernel.h>
#include <utility>

namespace htr {
HeatTransfer::HeatTransfer(int width, int height)
    : _width(width), _height(height), temperature(width, height),
      temperature_1(width, height) {
  glGenBuffers(1, &pixelBuffer);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pixelBuffer);
  glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, _width * _height * 4, nullptr,
               GL_DYNAMIC_DRAW);

  cudaGraphicsGLRegisterBuffer(&cudaMappedBuffer, pixelBuffer,
                               cudaGraphicsMapFlagsNone);
}

void HeatTransfer::reset() { krn::reset_kernel(temperature); }

void HeatTransfer::update(int sourcex, int sourcey) {
  for (int i = 0; i < iterationsPerFrame; ++i) {
    krn::run_kernel(temperature, temperature_1, sourcex, sourcey);
    std::swap(temperature, temperature_1);
  }
}

void HeatTransfer::draw() {
  uchar4 *mappedData;
  cudaGraphicsMapResources(1, &cudaMappedBuffer, nullptr);
  cudaGraphicsResourceGetMappedPointer((void **)&mappedData, nullptr,
                                       cudaMappedBuffer);
  krn::visualize_kernel(mappedData, temperature);
  cudaGraphicsUnmapResources(1, &cudaMappedBuffer, nullptr);

  glDisable(GL_BLEND);
  glDisable(GL_SCISSOR_TEST);

  glUseProgram(0);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pixelBuffer);
  glDrawPixels(_width, _height, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
}
} // namespace htr
