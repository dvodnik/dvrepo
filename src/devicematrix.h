#ifndef __INCLUDED_DEVICEMATRIX_H__
#define __INCLUDED_DEVICEMATRIX_H__
#include <cuda_runtime.h>

struct DeviceMatrix {
  float *data;
  int width, height;

  DeviceMatrix(int w, int h) : width(w), height(h) {
    cudaMalloc(&data, width * height * sizeof(float));
  }

  //~DeviceMatrix() {
  //	cudaFree(data);
  //}
};
#endif
